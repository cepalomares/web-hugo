---
title: Presentación de la OfiLibre
date: 2019-04-01
feature: Ofilibre-presentacion.png
teaser: Ofilibre-presentacion.png

transpas:
    pdf: Ofilibre-presentacion.pdf
    odp: Ofilibre-presentacion.odp

type: pres
---

Presentación de la Ofilibre, realizada en varios campus.
